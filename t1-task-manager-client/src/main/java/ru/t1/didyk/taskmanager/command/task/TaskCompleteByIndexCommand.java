package ru.t1.didyk.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.TaskCompleteByIndexRequest;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-complete-by-index";
    @NotNull
    public static final String DESCRIPTION = "Complete task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(getToken());
        request.setIndex(index);
        serviceLocator.getTaskEndpointClient().completeTaskByIndex(request);
    }

}
