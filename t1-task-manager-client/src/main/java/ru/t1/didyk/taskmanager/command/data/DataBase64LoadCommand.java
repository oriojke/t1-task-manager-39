package ru.t1.didyk.taskmanager.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.request.DataBase64LoadRequest;
import ru.t1.didyk.taskmanager.enumerated.Role;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-b64-load";
    @NotNull
    public static final String DESCRIPTION = "Load data from base64 file";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(getToken());
        serviceLocator.getDomainEndpointClient().domainBase64Load(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
