package ru.t1.didyk.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-unbind-from-project";
    @NotNull
    public static final String DESCRIPTION = "Unbind task to project.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        serviceLocator.getTaskEndpointClient().unbindFromProject(request);
    }

}
