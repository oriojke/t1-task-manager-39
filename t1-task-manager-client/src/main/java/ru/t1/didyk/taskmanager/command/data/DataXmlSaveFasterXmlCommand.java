package ru.t1.didyk.taskmanager.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.request.DataXmlSaveFasterXmlRequest;
import ru.t1.didyk.taskmanager.enumerated.Role;

public class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-xml-save-fasterxml";
    @NotNull
    public static final String DESCRIPTION = "Save data to xml file";

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(getToken());
        serviceLocator.getDomainEndpointClient().domainXmlSaveFasterXml(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
