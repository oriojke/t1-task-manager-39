package ru.t1.didyk.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.User;

import java.util.List;

public interface IUserRepository {

    @Nullable
    @Select("select * from users where login=#{login} limit 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "is_locked")
    })
    User findByLogin(@NotNull @Param(value = "login") String login);

    @Nullable
    @Select("select * from users where email=#{email} limit 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "is_locked")
    })
    User findByEmail(@NotNull @Param(value = "email") String email);

    @Delete("delete from users")
    void clear();

    @Select("select * from users")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "is_locked")
    })
    @NotNull List<User> findAll();

    @Insert("insert into users (id, first_name, middle_name, last_name, login, password, email, is_locked, role)" +
            "values (#{id}, #{firstName}, #{middleName}, #{lastName}, #{login}, #{passwordHash}, #{email}, #{locked}, #{role})")
    void add(@Nullable User model);

    @Select("select * from users where id=#{id} limit 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "is_locked")
    })
    @Nullable User findOneById(@NotNull @Param(value = "id") String id);

    @Select("select count(*) from users")
    int getSize();

    @Delete("delete from users where id=#{id}")
    void remove(@Nullable User model);

    @Delete("delete from users where id=#{id}")
    void removeById(@NotNull @Param(value = "id") String id);

    @Update("update users set first_name=#{firstName}, middle_name= #{middleName}, last_name=#{lastName}, login=#{login}, " +
            "password=#{passwordHash}, email=#{email}, is_locked=#{locked}, role=#{role} " +
            "where id=#{id}")
    void update(final @NotNull User object);
}
